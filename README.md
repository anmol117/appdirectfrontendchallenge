# appdirect-code-challenge-solution

Front End Coding Challenge

## Setup

Install the project dependencies:

`npm install`

## Running

Starts the static and proxy servers:

`npm start`

